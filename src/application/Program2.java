package application;

import java.util.List;
import java.util.Scanner;

import model.dao.ClienteDao;
import model.dao.DaoFactory;
import model.dao.LojaDao;
import model.entities.Loja;
import model.entities.Cliente;


public class Program2 {

		public static void main(String[] args) {

			Scanner sc = new Scanner(System.in);
			
			LojaDao lojaDao = DaoFactory.createLojaDao();		
			
			System.out.println("\nListando Lojas do Grupo :");
			List<Loja> list = lojaDao.findAll();
			for (Loja l : list) {
				System.out.println(l);
			}
			
			ClienteDao clienteDao = DaoFactory.createClienteDao();			
			
			System.out.println("\nListando Clientes :");
			List<Cliente> lists = clienteDao.findAll();
			for (Cliente c : lists) {
				System.out.println(c);
			}
			
			sc.close();
		}
}
