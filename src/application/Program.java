package application;

import java.util.Scanner;

import model.dao.DaoFactory;
import model.dao.ClienteDao;
import model.entities.Loja;
import model.entities.Cliente;


public class Program {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		ClienteDao clienteDao = DaoFactory.createClienteDao();
		
		int CodLoja = 0;
		
		while (CodLoja>4 || CodLoja<=0){
			System.out.print("=== Entre com o número da loja entre 1 e 4 =====:\n");
			CodLoja = sc.nextInt();
				}		
		
		System.out.print("=== Entre com o nome do cliente =====:\n");	
	    String NomeCliente = sc.next();
	    sc.nextLine();
	    
		System.out.print("=== Entre com o Cidade do cliente =====:\n");	
	    String CidadeCliente = sc.next();
	    sc.nextLine();
	    
		System.out.print("=== Entre com o Estado do cliente =====:\n");	
	    String EstadoCliente = sc.next();
	    sc.nextLine();
	    
		System.out.println("Loja Digitada: " + CodLoja + "Cliente:" + NomeCliente + "CidadeCliente:" + CidadeCliente + "EstadoCliente:" + EstadoCliente);
			
		Loja loja = new Loja(CodLoja, null);

		System.out.println("\n=== Inserindo Clientes =====");
		Cliente newCliente = new Cliente(null,NomeCliente, CidadeCliente, EstadoCliente, loja);

		clienteDao.insert(newCliente);
		System.out.println("Inserido! Novo Código = " + newCliente.getId());
		
		sc.close();
	}
}
	