package model.entities;

import java.io.Serializable;

public class Loja implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer idLoja;
	private String nomeLoja;
	
	public Loja() {
	}

	public Loja(Integer idLoja, String nomeLoja) {
		this.idLoja = idLoja;
		this.nomeLoja = nomeLoja;
	}

	public Integer getIdLoja() {
		return idLoja;
	}

	public void setIdLoja(Integer idLoja) {
		this.idLoja = idLoja;
	}

	public String getnomeLoja() {
		return nomeLoja;
	}

	public void setNomeLoja(String nomeLoja) {
		this.nomeLoja = nomeLoja;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idLoja == null) ? 0 : idLoja.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Loja other = (Loja) obj;
		if (idLoja == null) {
			if (other.idLoja != null)
				return false;
		} else if (!idLoja.equals(other.idLoja))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Loja: " + nomeLoja + " - Código da Loja  = " + idLoja;
	}
}