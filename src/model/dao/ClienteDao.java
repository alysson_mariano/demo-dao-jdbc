package model.dao;

import java.util.List;
import model.entities.Loja;
import model.entities.Cliente;

public interface ClienteDao {

	void insert(Cliente obj);
	Cliente findByIdLoja(Loja loja);
	List<Cliente> findAll();
	List<Cliente> findByLoja(Loja loja);
}