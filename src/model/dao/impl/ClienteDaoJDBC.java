package model.dao.impl;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import com.mysql.jdbc.Statement;

import db.DB;
import db.DbException;
import model.dao.ClienteDao;
import model.entities.Loja;
import model.entities.Cliente;

public class ClienteDaoJDBC implements ClienteDao {

	private Connection conn;
	
	public ClienteDaoJDBC(Connection conn) {
		this.conn = conn;
	}

	@Override
	public void insert(Cliente obj) {
		PreparedStatement st = null;
		try {
			st = conn.prepareStatement(
					"INSERT INTO cliente "
					+ "(Nome_Cliente, Cidade, Estado, IdLoja) "
					+ "VALUES "
					+ "(?, ?, ?, ?)",
					Statement.RETURN_GENERATED_KEYS);
			
			st.setString(1, obj.getNomeCliente());
			st.setString(2, obj.getCidade());
			st.setString(3, obj.getEstado());
			st.setInt(4, obj.getLoja().getIdLoja());
			
			int rowsAffected = st.executeUpdate();
			
			if (rowsAffected > 0) {
				ResultSet rs = st.getGeneratedKeys();
				if (rs.next()) {
					int id = rs.getInt(1);
					obj.setId(id);
				}
				DB.closeResultSet(rs);
			}
			else {
				throw new DbException("Erro inesperado! Colunas não alteradas!");
			}
		}
		catch (SQLException e) {
			throw new DbException(e.getMessage());
		}
		finally {
			DB.closeStatement(st);
		}
	}


	private Cliente instantiateCliente(ResultSet rs, Loja loj) throws SQLException {
		Cliente obj = new Cliente();
		obj.setId(rs.getInt("Id"));
		obj.setNomeCliente(rs.getString("Nome_Cliente"));
		obj.setCidade(rs.getString("Cidade"));
		obj.setEstado(rs.getString("Estado"));
		obj.setLoja(loj);
		return obj;
	}

	private Loja instantiateLoja(ResultSet rs) throws SQLException {
		Loja loj = new Loja();
		loj.setIdLoja(rs.getInt("idLoja"));
		loj.setNomeLoja(rs.getString("nomeLoja"));
		return loj;
	}

	@Override
	public List<Cliente> findAll() {
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = conn.prepareStatement(
					"SELECT cliente.*,loja.Nome_Loja as nomeLoja "
					+ "FROM cliente INNER JOIN loja "
					+ "ON cliente.IdLoja = loja.IdLoja "
					+ "ORDER BY Nome_Cliente");
			
			rs = st.executeQuery();
			
			List<Cliente> list = new ArrayList<>();
			Map<Integer, Loja> map = new HashMap<>();
			
			while (rs.next()) {
				
				Loja loj = map.get(rs.getInt("IdLoja"));
				
				if (loj == null) {
					loj = instantiateLoja(rs);
					map.put(rs.getInt("IdLoja"), loj);
				}
				
				Cliente obj = instantiateCliente(rs, loj);
				list.add(obj);
			}
			return list;
		}
		catch (SQLException e) {
			throw new DbException(e.getMessage());
		}
		finally {
			DB.closeStatement(st);
			DB.closeResultSet(rs);
		}
	}
	

	
	@Override
	public Cliente findByIdLoja(Loja loja) {
		return null;
	}
	
	@Override
	public List<Cliente> findByLoja(Loja loja) {
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = conn.prepareStatement(
					"SELECT cliente.*,loja.Nome_Loja as NomeLoja "
					+ "FROM cliente INNER JOIN loja "
					+ "ON cliente.IdLoja = loja.IdLoja "
					+ "WHERE cliente.idLoja = ? "
					+ "ORDER BY Nome_Cliente");
			
			st.setInt(1, loja.getIdLoja());
			
			rs = st.executeQuery();
			
			List<Cliente> list = new ArrayList<>();
			Map<Integer, Loja> map = new HashMap<>();
			
			while (rs.next()) {
				
				Loja loj = map.get(rs.getInt("idLoja"));
				
				if (loj == null) {
					loj = instantiateLoja(rs);
					map.put(rs.getInt("idLoja"), loj);
				}
				
				Cliente obj = instantiateCliente(rs, loj);
				list.add(obj);
			}
			return list;
		}
		catch (SQLException e) {
			throw new DbException(e.getMessage());
		}
		finally {
			DB.closeStatement(st);
			DB.closeResultSet(rs);
		}
	}
}