package model.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import db.DB;
import db.DbException;
//import db.DbIntegrityException;
import model.dao.LojaDao;
import model.entities.Loja;

public class LojaDaoJDBC implements LojaDao {

	private Connection conn;
	
	public LojaDaoJDBC(Connection conn) {
		this.conn = conn;
	}
	
	@Override
	public Loja findByIdLoja(Integer idLoja) {
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = conn.prepareStatement(
				"SELECT * FROM loja WHERE IdLoja = ? order by Nome_Loja");
			st.setInt(1, idLoja);
			rs = st.executeQuery();
			if (rs.next()) {
				Loja obj = new Loja();
				obj.setIdLoja(rs.getInt("idLoja"));
				obj.setNomeLoja(rs.getString("nomeLoja"));
				return obj;
			}
			return null;
		}
		catch (SQLException e) {
			throw new DbException(e.getMessage());
		}
		finally {
			DB.closeStatement(st);
			DB.closeResultSet(rs);
		}
	}

	@Override
	public List<Loja> findAll() {
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = conn.prepareStatement(
				"SELECT * FROM loja ORDER BY Nome_Loja");
			rs = st.executeQuery();

			List<Loja> list = new ArrayList<>();

			while (rs.next()) {
				Loja obj = new Loja();
				obj.setIdLoja(rs.getInt("IdLoja"));
				obj.setNomeLoja(rs.getString("Nome_Loja"));
				list.add(obj);
			}
			return list;
		}
		catch (SQLException e) {
			throw new DbException(e.getMessage());
		}
		finally {
			DB.closeStatement(st);
			DB.closeResultSet(rs);
		}
	}

	@Override
	public void insert(Loja obj) {
		PreparedStatement st = null;
		try {
			st = conn.prepareStatement(
				"INSERT INTO loja " +
				"(NomeLoja) " +
				"VALUES " +
				"(?)", 
				Statement.RETURN_GENERATED_KEYS);

			st.setString(1, obj.getnomeLoja());

			int rowsAffected = st.executeUpdate();
			
			if (rowsAffected > 0) {
				ResultSet rs = st.getGeneratedKeys();
				if (rs.next()) {
					int idLoja = rs.getInt(1);
					obj.setIdLoja(idLoja);
				}
			}
			else {
				throw new DbException("Erro inesperado. Linhas não alteradas!");
			}
		}
		catch (SQLException e) {
			throw new DbException(e.getMessage());
		} 
		finally {
			DB.closeStatement(st);
		}
	}

}