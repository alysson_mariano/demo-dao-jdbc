package model.dao;

import db.DB;
import model.dao.impl.LojaDaoJDBC;
import model.dao.impl.ClienteDaoJDBC;

public class DaoFactory {

	public static ClienteDao createClienteDao() {
		return new ClienteDaoJDBC(DB.getConnection());
	}
	
	public static LojaDao createLojaDao() {
		return new LojaDaoJDBC(DB.getConnection());
	}
}