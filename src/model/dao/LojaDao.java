package model.dao;

import java.util.List;

import model.entities.Loja;

public interface LojaDao {

	void insert(Loja obj);
	Loja findByIdLoja(Integer id);
	List<Loja> findAll();
}