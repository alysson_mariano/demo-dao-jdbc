CREATE TABLE loja (
  IdLoja int(11) NOT NULL AUTO_INCREMENT,
  Nome_Loja varchar(60) DEFAULT NULL,
  PRIMARY KEY (IdLoja)
);

CREATE TABLE cliente (
  Id int(11) NOT NULL AUTO_INCREMENT,
  Nome_Cliente varchar(60) NOT NULL,
  Cidade varchar(100) NOT NULL,
  Estado varchar(100) NOT NULL,
  IdLoja int(11) NOT NULL,
  PRIMARY KEY (Id),
  FOREIGN KEY (IdLoja) REFERENCES loja (IdLoja)
);

INSERT INTO loja (IdLoja,Nome_Loja) VALUES 
  (1,'Sudeste'),
  (2,'Sul'),
  (3,'Nordeste'),
  (4,'Centroeste');

INSERT INTO cliente (Nome_Cliente, Cidade, Estado, IdLoja) VALUES 
  ('Jandira Oliveira','Betim','Minas Gerais',1),
  ('Maria Tavares','Porto Alegre','Rio Grande do Sul',2),
  ('Mario Fontanna','Maceió','Alagoas',3),
  ('Janderson Vilaça','Campo Grande','Mato Grosso do Sul',4);
